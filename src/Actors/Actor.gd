extends KinematicBody2D
class_name Actor

export var max_speed: = 140.0
export var speed_coeff: = 1.0
export var max_jump_velocity: = 200.0
export var max_jump_height: = 40.0
export var attack_strike_dash: = 100.0
export var jump: = 300.0
export var gravity: = 1000.0
export var pv: = 100
export var damages: = 10
export var gravity_sensible: = true

export var bump_impulse: = Vector2(200.0, 0.0)
export var attack_impulse: = Vector2(300.0, 0.0)
export var dash_impulse: = 300.0

enum State { RESET, IDLE, RUN, JUMP, FALL, CHARGE_ATTACK, ATTACK, BUMPED, DASH, DIE }

onready var sprite: = $Sprite
onready var _is_attacking: = false
onready var _is_dashing: = false
onready var _is_jumping: = false
onready var _is_dying: = false
onready var _is_bumped: = false
onready var _is_charging_attack: = false
onready var _is_sprite_blinking: = false
onready var _jump_init_height: = 0.0
onready var _max_jump_height_reached: = false
onready var _attack_box_shape: = get_node_or_null("AttackBox/AttackBoxShape2D")
onready var _hit_particles: = $HitParticles
onready var _material = $Sprite.material

var _velocity: = Vector2.ZERO

var _attack_combo: = 0
var _attack_vel: = 0.0 # old _attack_strike
var _dash_vel: = 0.0 # old _dash_current
var _bump_vel: = Vector2.ZERO
var _x_dir: = 0.0
var _speed: = 0.0
var _last_x_direction: = 1

var can_jump: = true
var can_move: = true
var can_attack: = true
var can_flip: = true
var can_dash: = true
var can_take_damage: = true

var _attack: = false
var _attack_pressed: = false
var _attack_dashing: = false
var _attack_from_ground: = false

var state = State.IDLE

func _ready() -> void:
	pass

func _physics_process(_delta: float) -> void:
	_get_state()
	_animate()
	_attack_process()
	if not _is_dashing: # don't update current direction if dashing ( => can not change dir while dashing )
		_save_current_direction()

	if is_on_floor():
		_max_jump_height_reached = false
	if pv <= 0:
		die()
	if gravity_sensible:
		_velocity = _calculate_move_velocity(_velocity, get_x_dir())
		_velocity = move_and_slide(_velocity, Vector2.UP) # main vector movement, normal vector ( to determine floor )

func _process(_delta: float) -> void:
	return

# move
func _calculate_move_velocity(
	_vel: Vector2,
	_dir: float
) -> Vector2:
	var _out: Vector2
	if _dir == 0.0:
		_speed = 0.0
	else:
		_speed += 40 # TODO: set by variable !
	if _is_bumped:
		_out = _bump_vel
	else:
		_out.x = _calculate_x_velocity(_vel.x)
		_out.y = _calculate_y_velocity(_vel.y)
	return _out

func set_x_dir(_dir: float = 0.0):
	_x_dir = _dir

func get_x_dir() -> float:
	return _x_dir

func _calculate_x_velocity(_x: float) -> float:
	if can_move and not _is_dying and not _is_dashing:
		return (_x_dir * (min(_speed, max_speed) * speed_coeff) + _last_x_direction * _attack_vel)
	elif _is_dashing:
		return _last_x_direction * _dash_vel
	else:
		return 0.0

# gravity / jump
func _calculate_y_velocity(_y: float) -> float:
	if gravity_sensible:
		if _is_jumping:
			if is_on_floor():
				_jump_init_height = global_position.y
			_y = -max_jump_velocity
			if (-(global_position.y - _jump_init_height) > max_jump_height):
				_max_jump_height_reached = true
			if _max_jump_height_reached:
				_is_jumping = false
			if is_on_ceiling():
				_is_jumping = false
		elif _is_attacking:
			_y = gravity / 1.5 * get_physics_process_delta_time()
		else:
			_y += gravity * get_physics_process_delta_time()
		return _y
	else:
		return 0.0

func _save_current_direction() -> void:
	if get_x_dir() < 0: _last_x_direction = -1; elif get_x_dir() > 0: _last_x_direction = 1

func is_direction_changed():
	return true if (_x_dir < 0 and _last_x_direction > 0) or (_x_dir > 0 and _last_x_direction < 0) else false

# states
func _get_state() -> void:
	if not _is_dying:
		if not _is_bumped:
			if not _is_attacking:
				if _velocity.x != 0.0 and _velocity.y == 0.0 and is_on_floor(): state = State.RUN
				elif _velocity.y < 0.0 and not is_on_floor(): state = State.JUMP
				elif _velocity.y >= 0.0 and not is_on_floor(): state = State.FALL
				elif _velocity.x == 0.0 and _velocity.y == 0.0 and is_on_floor(): state = State.IDLE
			elif _is_attacking:
				state = State.ATTACK
			else:
				state = State.IDLE
		else:
			state = State.BUMPED
		if _is_dashing:
			state = State.DASH
	else:
		state = State.DIE

# attack
func _attack_process() -> void:
	if _attack and _attack_combo < 3 and can_attack:
		_attack_pressed = true
		_is_attacking = true
		_attack_from_ground = is_on_floor()
		_increase_attack_combo()
		$AttackComboTimer.start()
	else:
		_attack_pressed = false

func make_attack() -> void:
	_attack = true

func stop_attack() -> void:
	_attack = false

func make_jump() -> void:
	if can_jump:
		_is_jumping = true

func stop_jump() -> void:
	_is_jumping = false

func make_dash() -> void:
	if can_dash:
		if is_on_floor():
			can_jump = false
			can_attack = false
			can_move = false
			can_take_damage = false
			_is_dashing = true
			_dash_vel = dash_impulse
			$DashTimer.start()

func _increase_attack_combo():
	_attack_combo += 1

func _attack_spawn():
	_is_attacking = true
	if _attack_from_ground:
		can_jump = false
	can_flip = false
	_attack_spawn_start()

func _attack_strike():
	_attack_spawn_end()
	_attack_strike_start()

func _attack_end():
	_attack_strike_end()
	_attack_die_start()

func _attack_spawn_start() -> void:
	if _attack_from_ground or is_on_floor():
		can_flip = true
	else:
		can_flip = false
	can_jump = false
	speed_coeff = 0.3

func _attack_spawn_end() -> void:
	can_flip = false

func _attack_strike_start() -> void:
	_attack_vel = attack_strike_dash
	_attack_box_spawn()

func _attack_strike_end() -> void:
	_attack_vel = 0.0
	can_jump = true
	_attack_box_die()

func _attack_die_start() -> void:
	if _attack_from_ground or is_on_floor():
		speed_coeff = 0.05

func _attack_die_end() -> void:
	speed_coeff = 1.0
	_is_attacking = false
	can_flip = true

func _attack_box_spawn() -> void:
	if _attack_box_shape: _attack_box_shape.disabled = false

func _attack_box_die() -> void:
	if _attack_box_shape: _attack_box_shape.disabled = true

func _attack_cancel() -> void:
	yield(get_tree().create_timer(0.05), "timeout") # seems to avoid attack cancels bugs
	_attack_spawn_end()
	_attack_strike_end()
	_attack_die_end()

func _animate() -> void:
	if state == State.RESET: play_animation_if_exists("RESET")
	if not state == State.DIE:
		if not state == State.ATTACK:
			if state == State.IDLE: play_animation_if_exists("idle")
			elif state == State.RUN: play_animation_if_exists("run")
			elif state == State.JUMP: play_animation_if_exists("jump")
			elif state == State.FALL: play_animation_if_exists("fall")
		elif state == State.ATTACK and _attack_pressed:
			if _attack_combo == 1: play_animation_if_exists("attack1")
			if _attack_combo == 2: play_animation_if_exists("attack2")
			if _attack_combo == 3: play_animation_if_exists("attack3")
		if state == State.CHARGE_ATTACK:
			play_animation_if_exists("charging_attack")
		if not _is_bumped:
			if can_flip:
				scale.x = scale.y * _last_x_direction

		else:
			play_animation_if_exists("bumped")
		if state == State.DASH:
			play_animation_if_exists("dash")
	elif state == State.DIE: play_animation_if_exists("die")
	if _is_sprite_blinking:
		$Sprite.set_modulate(Color(1,1,1,0.5))

func touched_by(
	_body: KinematicBody2D,
	_groups_of_touchers: String,
	_invulnerability: bool = false ) -> void:
	if _body.is_in_group(_groups_of_touchers):
		if can_take_damage:
			_hit_particles.direction.x = -get_node_direction_side(_body)
			_hit_particles.emitting = true
			take_damage_from(_body)
			bump(get_node_direction_side(_body))
			if _invulnerability: invulnerability_on_damage()

func bump(_p_side: int) -> void:
	_attack_cancel()
	_material.set_shader_param("active", true)
	Globals.camera.shake(50)
	$BumpedTimer.start()
	$NoMoveWhenBumpedTimer.start()
	EventBus.emit_signal("enemy_hit")
	_is_bumped = true
	can_move = false
	can_jump = false
	can_attack = false
	_bump_vel.x = -_p_side * bump_impulse.x
	_bump_vel.y = bump_impulse.y

func _on_BumpedTimer_timeout():
	_material.set_shader_param("active", false)
	_is_bumped = false
	can_jump = true
	can_attack = true

func take_damage_from(_a: PhysicsBody2D) -> void:
	pv -= _a.damages

func invulnerability_on_damage() -> void:
	if $InvulnerabilityTimer.is_stopped():
		$InvulnerabilityTimer.start()
		can_take_damage = false
		_is_sprite_blinking = true

func _on_InvulnerabilityTimer_timeout():
	can_take_damage = true
	$Sprite.set_modulate(Color(1,1,1,1))
	_is_sprite_blinking = false

func play_animation_if_exists(_anim: String) -> void:
	if $AnimationPlayer.has_animation(_anim): $AnimationPlayer.play(_anim)

func get_node_direction_side(_n: Node2D) -> int:
	if _n.global_position.x < global_position.x: return -1; else: return 1;

func die() -> void:
	_is_bumped = false
	_is_dying = true
	can_move = false
	can_take_damage = false

func is_dying() -> bool:
	return _is_dying

func _free() -> void:
	yield(get_tree().create_timer(0.5), "timeout")
	queue_free()

func _on_BodyDetector_body_entered(_body: KinematicBody2D):
	pass

func _on_BodyDetector_body_exited(_body: KinematicBody2D):
	pass

func _on_NoMoveWhenBumpedTimer_timeout():
	can_move = true

func _on_AttackComboTimer_timeout():
	_attack_combo = 0
