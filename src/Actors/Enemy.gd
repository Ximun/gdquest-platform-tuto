extends Actor

var follow_player: = false

func _physics_process(_delta: float) -> void:
	if follow_player: set_x_dir(get_node_direction_side(Globals.player))
	else: set_x_dir(0)

func _on_PlayerAttackBoxDetector_area_entered(area: Area2D) -> void:
	if area.get_parent() == Globals.player and area.name == "AttackBox":
		touched_by(Globals.player, "player")

func die() -> void:
	.die()

func _on_PlayerDetector_body_entered(_body: KinematicBody2D):
	if _body == Globals.player: follow_player = true

func _on_PlayerDetector_body_exited(_body: KinematicBody2D):
	if _body == Globals.player: follow_player = false
