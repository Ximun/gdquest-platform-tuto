extends Actor

onready var player = get_tree().get_root().get_node("LevelTemplate/Player")

#func _ready() -> void:
#	_speed = speed_walking
#	set_physics_process(false) # _physics_process() not running before enemy is in the view

#func _physics_process(delta: float) -> void:
#	if not _bumped and is_on_floor():
#		if is_on_wall():
#			direction.x *= -1.0
#		if _follow_player: direction.x = _get_body_direction_side(player)
#		_velocity.x = _speed * direction.x
#	_velocity.y += gravity * delta
#	if _bumped:
#		_velocity.x = -_get_body_direction_side(player) * 500.0
#	_velocity.y = move_and_slide(_velocity, Vector2.UP).y
#	if pv <= 0:
#		#die()
#		pass

#func _process(delta: float) -> void:
#	if state == State.RUN:
#		$AnimationPlayer.play("walk")
#	elif state == State.ATTACK:
#		$AnimationPlayer.play("attack")
#	elif state == State.DIE:
#		$AnimationPlayer.play("die")
#	if _velocity.x > 0.0:
#		$Sprite.set_flip_h(true)
#	else:
#		$Sprite.set_flip_h(false)

func _process(delta: float) -> void:
	set_x_dir(1.0)

#func _get_state():
#	pass
#
#func _on_PlayerAttackBoxDetector_area_entered(area):
#	if area.name == "AttackBox":
#		_bumped = true
#		$BumpedTimer.start()
#		_take_damage()
#
#func _take_damage() -> void:
#	pv -= 10
#
#func _on_BumpedTimer_timeout():
#	_bumped = false
#
#func _get_body_direction_side(body: PhysicsBody2D) -> int:
#	if body.global_position.x < global_position.x:
#		return -1
#	else:
#		return 1
#
#func _on_PlayerDetector_body_entered(body):
#	if body.name == "Player":
#		state = State.ATTACK
#		_speed = speed_attacking
#		_follow_player = true
#		direction.x = _get_body_direction_side(body)
#
#func _on_PlayerDetector_body_exited(body):
#	if body.name == "Player":
#		state = State.RUN
#		_speed = speed_walking
#
#func die() -> void:
#	state = State.DIE
