extends "res://src/Actors/Ennemies/Projectiler.gd"

func _process(delta):
	pass

# Override all _animate function because sprite direction based on player direction
func _animate() -> void:
	if state == State.RESET: play_animation_if_exists("RESET")
	if not state == State.DIE:
		if not state == State.ATTACK:
			if state == State.IDLE: play_animation_if_exists("idle")
			elif state == State.RUN: play_animation_if_exists("run")
			elif state == State.JUMP: play_animation_if_exists("jump")
			elif state == State.FALL: play_animation_if_exists("fall")
		elif state == State.ATTACK and _attack_pressed:
			if _attack_combo == 1: play_animation_if_exists("attack1")
			if _attack_combo == 2: play_animation_if_exists("attack2")
			if _attack_combo == 3: play_animation_if_exists("attack3")
		if state == State.CHARGE_ATTACK:
			play_animation_if_exists("charging_attack")
		if not _is_bumped:
			if can_flip:
				if get_node_direction_side(Globals.player) < 0.0:
					$Sprite.set_flip_h(false)
					if get_node_or_null("AttackBox"): $AttackBox.scale.x = 1
				if get_node_direction_side(Globals.player) > 0.0:
					$Sprite.set_flip_h(true)
					if get_node_or_null("AttackBox"): $AttackBox.scale.x = -1
		else:
			play_animation_if_exists("bumped")
		if state == State.DASH:
			play_animation_if_exists("dash")
	elif state == State.DIE: play_animation_if_exists("die")
	if _is_sprite_blinking:
		$Sprite.set_modulate(Color(1,1,1,0.5))
