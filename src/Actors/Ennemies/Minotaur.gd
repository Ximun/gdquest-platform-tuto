extends "res://src/Actors/Enemy.gd"

onready var _attack_player_ray_cast = $AttackPlayerRayCast2D

export var distance_min_attack:= 20

func _process(_delta: float) -> void:
	if _attack_player_ray_cast.is_colliding(): make_attack(); else: stop_attack()
