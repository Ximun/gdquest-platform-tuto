extends "res://src/Actors/Enemy.gd"

#onready var TW = Tween.new()

var _coeff:= Vector2(0,0)
var direction: Vector2
var distance_with_player: float

export var duration_pre_shouting: = 1.0

export var distance_player_go_backward: = 55.0 # if distance with player < this value, enemy go backward from player
export var distance_player_go_forward: = 85.0 # if distance with player > this value, enemy go forward to player
export var distance_player_slow_down: = 105.0 # if distance with player < this value, enemy go slower
export var acceleration_curve: Curve # acceleration

export var slow_down_coeff: = 0.2

func _process(delta: float) -> void:
	if $AttackTimer.is_stopped():
		$AttackTimer.start()
	distance_with_player = Vector2(get_position()).distance_to(Globals.player.global_position)
	direction = Vector2.direction_to(Globals.player.global_position-global_position).normalized()
	if can_move:
		if distance_with_player < distance_player_go_backward:
			# set vector direction inverse de direction_to(player)
			#_velocity = lerp(_velocity,-Vector2.direction_to(player.global_position-global_position), movement_curve.interpolate(distance_with_player))
#			_velocity.x = movement_curve.interpolate(-(player.global_position.x-global_position.x))
#			_velocity.y = movement_curve.interpolate(-(player.global_position.y-global_position.y))
		#elif distance_with_player == $PlayerDetector/PlayerDetectorShape2D.shape.radius:
	#	elif distance_with_player == 100:
	#		_velocity = Vector2.ZERO
			_velocity = -direction * Vector2(acceleration_curve.interpolate(_coeff.x),acceleration_curve.interpolate(_coeff.y))
		elif distance_with_player > distance_player_go_forward:
			#_velocity = lerp(_velocity,Vector2.direction_to(player.global_position-global_position), 0.05)
			#_velocity = lerp(_velocity,-Vector2.direction_to(player.global_position-global_position), movement_curve.interpolate(distance_with_player))
#			_velocity.x = movement_curve.interpolate(player.global_position.x-global_position.x)
#			_velocity.y = movement_curve.interpolate(player.global_position.y-global_position.y)
			#_velocity = Vector2.direction_to(player.global_position-global_position) * max_speed * movement_curve.interpolate(max(_coeff,1))
			_velocity = direction * Vector2(acceleration_curve.interpolate(_coeff.x),acceleration_curve.interpolate(_coeff.y))
#		else:
#			print("yo")
#			_velocity = direction * Vector2(movement_curve.interpolate(_coeff.x),movement_curve.interpolate(_coeff.y))
	else:
		_velocity = Vector2.ZERO

func _physics_process(delta: float) -> void:
	if direction.x == 0.0:
		_coeff.x = 0
	elif direction.y == 0.0:
		_coeff.y = 0
	elif distance_with_player <= distance_player_slow_down and distance_with_player >= distance_player_go_backward:
		_coeff = Vector2(slow_down_coeff,slow_down_coeff)
	else:
		_coeff.x += 0.5 * delta
		_coeff.y += 0.5 * delta
	if _is_bumped:
		move_and_slide(_bump_vel)
	else:
		move_and_slide(_velocity*max_speed)

# states
func _get_state() -> void:
	if not _is_dying:
		if not _is_bumped:
			if not _is_attacking:
				if _velocity.x != 0.0 or _velocity.y != 0.0 and is_on_floor(): state = State.RUN
				elif _velocity.x == 0.0 and _velocity.y == 0.0 and is_on_floor(): state = State.IDLE
			elif _is_attacking:
				state = State.ATTACK
			else:
				state = State.IDLE
		else:
			state = State.BUMPED
		if _is_charging_attack:
			state = State.CHARGE_ATTACK
	else:
		state = State.DIE

func throw_projectile(_target: Node2D) -> void:
	#$Sprite.set_modulate(Color(0,0,0,1))
	_is_charging_attack = true
	yield(get_tree().create_timer(duration_pre_shouting), "timeout")
	_is_charging_attack = false
	#$Sprite.set_modulate(Color(1,1,1,1))
	# Projectile instanciation and launch
	var _ball = preload("res://src/Objects/Projectiles/Ball.tscn").instance()
	_ball.direction = Vector2.direction_to(Globals.player.global_position-global_position)
	_ball.damages = self.damages
	_ball.set_position(self.global_position)
	get_tree().get_root().add_child(_ball)


func _on_AttackTimer_timeout():
	throw_projectile(Globals.player)
