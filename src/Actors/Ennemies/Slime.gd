extends "res://src/Actors/Enemy.gd"

func _ready():
	set_x_dir(1.0)

func _physics_process(delta: float) -> void:
	if is_on_wall(): set_x_dir(-get_x_dir())

func _on_PlayerDetector_body_entered(body):
	if body == Globals.player:
		speed_coeff = 2
		follow_player = true

func _on_PlayerDetector_body_exited(body):
	if body == Globals.player:
		speed_coeff = 1
		follow_player = false
