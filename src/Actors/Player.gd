extends Actor

func _ready() -> void:
	Globals.player = self

func _physics_process(delta):
	set_x_dir(get_player_direction())
	if Input.is_action_just_pressed("jump"): make_jump()
	if Input.is_action_just_released("jump"): stop_jump()
	if Input.is_action_just_pressed("attack"): make_attack(); else: stop_attack()
	if Input.is_action_just_pressed("dash"): make_dash()
	if _is_attacking or _is_dashing:
		can_dash = false
	else:
		can_dash = true

func get_player_direction() -> float:
	return Input.get_action_strength("move_right") - Input.get_action_strength("move_left")

func calculate_bump_velocity(linear_velocity: Vector2, impulse: Vector2, position: float) -> Vector2:
	var out: = linear_velocity
	var xpos: = position < global_position.x
	if xpos:
		out.x =  impulse.x
	else:
		out.x = -impulse.x
	out.y = -impulse.y
	return out

func _on_DashTimer_timeout():
	_dash_vel = 0.0
	_is_dashing = false
	can_move = true
	can_jump = true
	can_attack = true
	can_dash = true
	can_take_damage = true

func _on_BodyDetector_body_entered(_body: KinematicBody2D):
	if _body.is_in_group("enemies"):
		if not _body.is_dying():
			touched_by(_body, "enemies", true)
	elif _body.is_in_group("projectiles"):
		touched_by(_body, "projectiles", true)
		_body.queue_free()

func _on_BodyDetector_body_exited(_body: KinematicBody2D):
	pass

func _on_EnemyAttackBoxDetector_area_entered(area: Area2D) -> void:
	if area.get_parent().is_in_group("enemies") and area.name == "AttackBox":
		touched_by(area.get_parent(), "enemies")
