extends Actor

export var dialog_timeline: String

var dialog_triggerable: bool
var dialog_instantiable: bool

func _ready() -> void:
	dialog_instantiable = true

func _input(event: InputEvent) -> void:
	if get_node_or_null("DialogNode") == null:
		if event.is_action_pressed("ui_up") and dialog_triggerable and dialog_instantiable:
			var dialog = Dialogic.start(dialog_timeline)
			dialog.connect("dialogic_signal", self, "_dialog_listener")
			add_child(dialog)
			dialog.connect("dialogic_signal", self, "_dialog_listener")

func _on_DialogArea_body_entered(body: KinematicBody2D) -> void:
	if body == Globals.player: dialog_triggerable = true

func _on_DialogArea_body_exited(body: KinematicBody2D) -> void:
	if body == Globals.player: dialog_triggerable = false

func _dialog_start() -> void:
	_flip_by_player_position()
	dialog_instantiable = false
	Globals.player.can_jump = false
	Globals.player.can_move = false

func _dialog_end() -> void:
	yield(get_tree().create_timer(0.005), "timeout") # Avoid jump while release
	dialog_instantiable = true
	Globals.player.can_move = true
	Globals.player.can_jump = true

func _dialog_listener(signal_name: String) -> void:
	match signal_name:
		"dialog_start": _dialog_start()
		"dialog_end": _dialog_end()

func _flip_by_player_position() -> void:
	if Globals.player.global_position.x < global_position.x:
		sprite.set_flip_h(true)
	else:
		sprite.set_flip_h(false)
