extends Actor

var player_position: int

onready var player_contact_dialog: = false

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

func _process(delta: float) -> void:
	$AnimationPlayer.play("idle")

func _input(event: InputEvent) -> void:
	if get_node_or_null("DialogNode") == null:
		if event.is_action_pressed("ui_up") and player_contact_dialog:
			var dialog = Dialogic.start("Samurai")
			dialog.connect("dialogic_signal", self, "_dialog_listener")
			add_child(dialog)
			dialog.connect("dialogic_signal", self, "_dialog_listener")

func _on_DialogArea_body_entered(body: KinematicBody2D) -> void:
	if body.name == "Player":
		player_contact_dialog = true

func _on_DialogArea_body_exited(body: KinematicBody2D):
	if body.name == "Player":
		player_contact_dialog = false

func _dialog_start() -> void:
	_flip_by_player_position()
	player.can_move = false 

func _dialog_end() -> void:
	yield(get_tree().create_timer(0.005), "timeout") # Avoid jump while release
	player.can_move = true

func _dialog_listener(signal_name: String) -> void:
	match signal_name:
		"dialog_start": _dialog_start()
		"dialog_end": _dialog_end()

func _flip_by_player_position() -> void:
	if player.global_position.x < global_position.x:
		sprite.set_flip_h(true)
	else:
		sprite.set_flip_h(false)
