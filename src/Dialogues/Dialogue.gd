extends CanvasLayer

export ( String, FILE, "*.json" ) var dialogue_file

var dialogues = []
var window: NinePatchRect

func _ready():
	window = $NinePatchRect
	window.visible = false
	play()

func play() -> void:
	dialogues = _load_dialogue()
	$NinePatchRect/RichTextLabel.text = dialogues[0]["text"]

func _load_dialogue() -> String:
	var file = File.new()
	if file.file_exists(dialogue_file):
		file.open(dialogue_file, file.READ)
		return parse_json(file.get_as_text())
	else:
		return "File not exists"
