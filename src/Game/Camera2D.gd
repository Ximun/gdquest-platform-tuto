extends Camera2D

onready var _shake_timer = $ShakeTimer
onready var _shake_tween = $ShakeTween

var _shake_amount = 0
var _default_offset = offset

func _ready() -> void:
	Globals.camera = self
	set_process(false)

func _process(_delta: float) -> void:
	offset = Vector2(rand_range(-_shake_amount, _shake_amount),0) * _delta + _default_offset

func shake(
		_shake,
		_time: float = 0.2,
		_limit: int = 100) -> void:
	_shake_amount += _shake
	if _shake_amount > _shake: _shake_amount = _limit

	_shake_timer.wait_time = _time

	_shake_tween.stop_all()
	set_process(true)
	_shake_timer.start()

func _on_ShakeTimer_timeout():
	_shake_amount = 0
	set_process(false)

	_shake_tween.interpolate_property(self, "offset", offset, _default_offset, 0.1, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	_shake_tween.start()
