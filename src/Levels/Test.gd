extends Node2D

var vector: = Vector2(50,0)
var position1: = Vector2(100,100)
var position2: = Vector2(300,100)

func _ready():
	pass # Replace with function body.

func _process(delta):
	print(vector)
	update()

func _draw():
	draw_line(position1,position1+vector,Color(255,255,0,1))

func _input(event):
	if event.is_action_pressed("ui_accept"):
		vector = vector.rotated(deg2rad(25))
