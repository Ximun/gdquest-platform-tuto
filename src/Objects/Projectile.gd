extends "res://src/Objects/Object.gd"

var direction: Vector2
var sender: PhysicsBody2D
var damages: float

export var speed: = 90.0


func _physics_process(delta: float) -> void:
	if is_on_wall() or is_on_floor():
		queue_free()
	var o = move_and_collide(direction*speed*delta)
	if o: if o.collider.name == "TileMap" or o.collider.name == "Structure" or o.collider.name == "Platforms": queue_free()
